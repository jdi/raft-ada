package Server_Second is
  type Server_Record (Id : Integer; Number_Of_Peers : Natural) is limited private;

  type Server_Record_Access is access Server_Record;

  type Peers_Array is array (Natural range <>) of Server_Record_Access;

  type Server_Status_Type is (Follower, Candidate, Leader);

  protected type Server_Protected (Id : Integer) is
    function Get_Server_Id return Integer;
    procedure Increase_Current_Term;
    function Get_Current_Term return Natural;
  private
    Server_Id : Integer := Id;
    Current_Term : Natural := 0;
    Current_Leader : Server_Record_Access := null;
    Server_Status : Server_Status_Type := Follower;
    Votes_In_Term : Natural := 0;
    Voted_For : Server_Record_Access := null;
  end Server_Protected;

  task type Server_Task (Id : Integer) is
    entry Request_Votes;
    entry Give_Vote (SP : Server_Protected);
  end Server_Task;

  type Server_Task_Access is access Server_Task;

  task type Election_Timer (ST : Server_Task_Access) is
    entry Start;
  end Election_Timer;

  function New_Server (Id : Integer; Number_Of_Peers : Natural) return Server_Record_Access;

  procedure Start_Server (SR : in Server_Record_Access);

  procedure Meet_Servers (SR1, SR2 : in out Server_Record_Access);
private
  type Server_Record (Id : Integer; Number_Of_Peers : Natural) is limited
    record
      ST : Server_Task_Access;
      Peers : Peers_Array (1 .. Number_Of_Peers);
    end record;
end Server_Second;
