with Ada.Text_IO;

package body Server_Second is
  protected body Server_Protected is
    function Get_Server_Id return Integer is
    begin
      return Server_Id;
    end Get_Server_Id;

    procedure Increase_Current_Term is
    begin
      Current_Term := Current_Term + 1;
    end Increase_Current_Term;

    function Get_Current_Term return Natural is
    begin
      return Current_Term;
    end Get_Current_Term;
  end Server_Protected;

  task body Server_Task is
    SP : Server_Protected (Id => Id);
  begin
    loop
      select
        accept Request_Votes do
          Ada.Text_IO.Put_Line (SP.Get_Server_Id'Image);
          -- SP.Increase_Current_Term;
        end Request_Votes;
      or
        accept Give_Vote (SP : Server_Protected);
      end select;
    end loop;
  end Server_Task;

  task body Election_Timer is
  begin
    loop
      accept Start;
      delay 0.5;
      ST.Request_Votes;
    end loop;
  end Election_Timer;

  function New_Server (Id : Integer; Number_Of_Peers : Natural) return Server_Record_Access is
  begin
    return new Server_Record'(Id => Id, Number_Of_Peers => Number_Of_Peers, others => <>);
  end;

  procedure Start_Server (SR : in Server_Record_Access) is
    ET : Election_Timer (SR.ST);
  begin
    ET.Start;
  end Start_Server;

  procedure Meet_Servers (SR1, SR2 : in out Server_Record_Access) is
  begin
    for P of SR1.Peers loop
      if P = null then
        P := SR2;
        exit;
      end if;
    end loop;

    for P of SR2.Peers loop
      if P = null then
        P := SR1;
        exit;
      end if;
    end loop;
  end Meet_Servers;
end Server_Second;
