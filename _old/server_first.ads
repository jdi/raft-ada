package Server_First is
  type Server_Task (Id : Integer; Number_Of_Peers : Natural);

  type Server_Task_Access is access Server_Task;

  type Peers_Array is array (Natural range <>) of Server_Task_Access;

  type Server_Kind is (Follower, Candidate, Leader);

  -- protected type Server_Record (Id : Integer; Number_Of_Peers : Natural) is

  -- private
  --   Server_Id : Integer := Id;
  --   Current_Term : Natural := 0;
  --   Current_Leader : Server_Task_Access := null;
  --   Voted_For : Server_Task_Access := null;
  --   Peers : Peers_Array(1 .. Number_Of_Peers);
  --   Kind : Server_Kind := Follower;
  --   Votes_In_Term : Natural := 0;
  --   All_Peers : Boolean := False;
  -- end Server_Record;

  task type Server_Task (Id : Integer; Number_Of_Peers : Natural) is
    entry Request_Votes;
    -- Request_Votes makes the Server_Task pass from the state Follower to Candidate and
    -- its activated everytime the timeout gets to zero without receiving any message from
    -- another server.
    -- It also ups by one the Server's Current_Term and Votes_In_Term (since every server
    -- that requests an election votes for itself)
    entry Concede_Vote (CT : in Natural; VS : in out Natural);
    entry Accept_Leader (Id : in Integer);
    entry Add_Peer (S : Server_Task_Access);
  end Server_Task;
end Server_First;
