with Ada.Containers;
with Ada.Containers.Hashed_Maps;
with Ada.Strings.Hash;

with Raft_Server;

package Raft_Peer is
    pragma Assertion_Policy (Check);

    type Peer is tagged private;

    type Peer_Access is access Peer;

    function Id_Hashes (Id : Natural) return Ada.Containers.Hash_Type;

    package Peers_Map is new Ada.Containers.Hashed_Maps
        (Key_Type => Natural,
        Element_Type => Peer_Access,
        Hash => Id_Hashes,
        Equivalent_Keys => "=");

    type Peers_Map_Access is access Peers_Map.Map;

    function New_Peer (Id : in Natural) return Peer_Access;

    procedure Set_Configuration (P : in out Peer; Cluster : in Peers_Map_Access);

    function Is_Configured (P : in Peer) return Boolean;

    procedure Start (P : in out Peer)
        with Pre => Is_Configured (P);

    procedure Stop (P : in Peer);
private
    type Peer is tagged
        record
            Id : Natural;
            Server : Raft_Server.Server_Access;
            Cluster : Peers_Map_Access := new Peers_Map.Map;
            Configured : Boolean := False;
        end record;
end Raft_Peer;
