with Ada.Text_IO;
with Ada.Numerics.Float_Random;

package body Server_First is
  task body Server_Task is
    Server_Id : Integer := Id;
    Current_Term : Natural := 0;
    Current_Leader : Server_Task_Access := null;
    Voted_For : Server_Task_Access := null;
    Peers : Peers_Array(1 .. Number_Of_Peers);
    Kind : Server_Kind := Follower;
    Votes_In_Term : Natural := 0;
    All_Peers : Boolean := False;

    Gen : Ada.Numerics.Float_Random.Generator;
  begin
    loop
      Ada.Numerics.Float_Random.Reset (Gen);
      delay Duration (Ada.Numerics.Float_Random.Random (Gen));
      select
        accept Add_Peer(S : Server_Task_Access) do
          for P of Peers loop
            if P = null then
              P := S;
              exit;
            end if;
          end loop;
          if Peers (Peers'Last) /= null then
            All_Peers := True;
          end if;
        end Add_Peer;
      or
        when Current_Leader = null =>
          accept Request_Votes do
            Ada.Text_IO.Put_Line ("Server" & Server_Id'Image & " initiated an election!");
            Current_Term := Current_Term + 1;
            Votes_In_Term := 1;
            for P of Peers loop
              P.Concede_Vote (Current_Term, Votes_In_Term);
            end loop;
            if Votes_In_Term >= Peers'Last / 2 then
              Kind := Leader;
              Ada.Text_IO.Put_Line ("Server" & Server_Id'Image & " became the leader!");
              for P of Peers loop
                P.Accept_Leader (Server_Id);
              end loop;
            else
              Ada.Text_IO.Put_Line ("Server" & Server_Id'Image & " failed to become the leader!");
            end if;
          end Request_Votes;
      or
        accept Concede_Vote (CT : in Natural; VS : in out Natural) do
          if CT > Current_Term then
            VS := VS + 1;
            Ada.Text_IO.Put_Line ("Server" & Server_Id'Image & " conceded its vote!");
          else
            Ada.Text_IO.Put_Line ("Server" & Server_Id'Image & " refused to concede its vote!");
          end if;
        end Concede_Vote;
      or
        accept Accept_Leader (Id : in Integer) do
          for P of Peers loop
            if P.Id = Id then
              Current_Leader := P;
              Ada.Text_IO.Put_Line ("Server" & Server_Id'Image & " accepted Server" & P.Id'Image & " as its leader!");
              exit;
            end if;
          end loop;
        end Accept_Leader;
      end select;
    end loop;
  end Server_Task;
end Server_First;
