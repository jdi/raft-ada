with Ada.Text_IO; use Ada.Text_IO;

package body Raft_Peer is
    function New_Peer (Id : in Natural) return Peer_Access is
    begin
        return new Peer'(Id => Id, others => <>);
    end New_Peer;

    procedure Set_Configuration (P : in out Peer; Cluster : in Peers_Map_Access) is
        Peers : Raft_Server.Peers_Map_Access := new Raft_Server.Peers_Hash.Map;
    begin
        for PM in Peers_Map.Iterate (Cluster.all) loop
            declare
                PM_Id : Natural := Peers_Map.Key (PM);
            begin
                case PM_Id = P.Id is
                    when True => null;
                    when False => P.Cluster.all.Insert (Key => PM_Id, New_Item => Cluster.all (PM));
                end case;
            end;
        end loop;
        for I in Peers_Map.Iterate (P.Cluster.all) loop
            Peers.Insert (Key => Peers_Map.Key (I), New_Item => P.Cluster.all (I).Server);
        end loop;
        P.Server := new Raft_Server.Server (P.Id, Peers, P.Id);
        P.Configured := True;
    end Set_Configuration;

    function Is_Configured (P : in Peer) return Boolean is
    begin
        return P.Configured;
    end Is_Configured;

    procedure Start (P : in out Peer) is
    begin
        P.Server.Start;
    end Start;

    procedure Stop (P : in Peer) is
    begin
        P.Server.Quit;
    end Stop;

    function Id_Hashes (Id : Natural) return Ada.Containers.Hash_Type is
    begin
        return Ada.Strings.Hash (Id'Image);
    end Id_Hashes;
end Raft_Peer;
