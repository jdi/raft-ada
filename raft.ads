with Ada.Numerics.Float_Random;
with Ada.Containers;
with Ada.Containers.Hashed_Maps;
with Ada.Containers.Vectors;
with Ada.Strings.Hash;
with GNATCOLL.Traces;

package Raft is
  type Log_Entry is
    record
      Term : Natural;
      Is_Empty : Boolean;
    end record;

  package Log_Vector is new Ada.Containers.Vectors (Natural, Log_Entry);

  type Role_Type is (Follower, Candidate, Leader);

  type Server;

  type Server_Access is access Server;

  type Peers_Type is array (Natural range <>) of Server_Access;

  function Id_Hashes (Id : Natural) return Ada.Containers.Hash_Type;

  package Peers_Hash is new Ada.Containers.Hashed_Maps
    (Key_Type => Natural,
     Element_Type => Server_Access,
     Hash => Id_Hashes,
     Equivalent_Keys => "=");

  package Next_Index_Type is new Ada.Containers.Hashed_Maps
    (Key_Type => Natural,
     Element_Type => Natural,
     Hash => Id_Hashes,
     Equivalent_Keys => "=");

  package Match_Index_Type is new Ada.Containers.Hashed_Maps
    (Key_Type => Natural,
     Element_Type => Natural,
     Hash => Id_Hashes,
     Equivalent_Keys => "=");

  type Peers_Map_Access is access Peers_Hash.Map;

  type Request_Vote_Response is
    record
      Term : Natural;
      Vote_Granted : Boolean;
    end record;

  type Append_Entries_Response is
    record
      Term : Natural;
      Success : Boolean;
    end record;

  task type Server (Id : Natural) is
    entry Set_Configuration (Cluster : in Peers_Map_Access);
    entry Start;
    entry Start_Election;
    entry Request_Vote (Term : in Natural; Candidate_Id : in Natural; Response : out Request_Vote_Response);
    entry Append_Entries (Term, Leader_Id, Prev_Log_Index, Prev_Log_Term : in Natural; E : in Log_Entry; Leader_Commit : in Natural; Response : out Append_Entries_Response);
    entry Issue_Command;
    entry Quit;
  end Server;

  -- procedure Issue_Command (Command : in Log_Entry; S : in Server_Access);

private
  function Reset_Ticker (G : in out Ada.Numerics.Float_Random.Generator; F : Float) return Duration;
  procedure Init_Next_Index (NI : in out Next_Index_Type.Map; Log : in Log_Vector.Vector; Peers : in Peers_Map_Access);
  function Get_Last_Log_Term (Log : Log_Vector.Vector) return Natural;
  function Get_Prev_Log_Index (NI : in Natural; Log : in Log_Vector.Vector) return Natural;
  function Get_Prev_Log_Term (NI : in Natural; Log : in Log_Vector.Vector) return Natural;
  function Get_Entry (NI : in Natural; Log : in Log_Vector.Vector) return Log_Entry;
  procedure Print_Log (Log : in Log_Vector.Vector; Stream : in GNATCOLL.Traces.Trace_Handle);
end Raft;
