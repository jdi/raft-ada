with Ada.Text_IO; use Ada.Text_IO;
with Ada.Exceptions;

package body Raft is
  G : Ada.Numerics.Float_Random.Generator;

  Quit_Duration : Duration := 3.0;

  -- Election_Duration : constant Float := 0.2;
  -- Heartbeat_Duration : constant Float := Election_Duration / 10.0;
  -- The Heartbeat_Duration must be orders of magnitude less
  -- than the Election_Duration.

  task body Server is
    Stream : constant GNATCOLL.Traces.Trace_Handle := GNATCOLL.Traces.Create ("Server" & Id'Image);

    Server_Id : Natural := Id;
    Role : Role_Type := Follower;
    Current_Term : Natural := 0;
    Current_Leader : Server_Access := null;
    Voted_For : Natural := 0;
    Peers : Peers_Map_Access := new Peers_Hash.Map;
    Log : Log_Vector.Vector;

    -- Volatile on all servers
    Commit_Index : Natural := 0;
    Last_Applied : Natural := 0;

    -- Volatile on leader
    Next_Index : Next_Index_Type.Map;
    Match_Index : Match_Index_Type.Map;

    Election_Duration : Float := Float (Server_Id) / 10.0;
    Heartbeat_Duration : Float := Election_Duration / 10.0;
    -- TODO: Temporal variables, must be randomized!

    task Election_Ticker is
    -- Election_Ticker is the task that coordinates the timer
    -- in which the Server should start an election.
      entry Start;
      entry Quit;
      entry Reset_Ticker;
      -- Reset_Ticker is just an entry for avoiding the task
      -- into falling inside the 'or' clause, thus effectively
      -- resetting the timer.
    end Election_Ticker;

    task body Election_Ticker is
      -- ET : Duration := Reset_Ticker (G, Election_Duration);
      ET : Duration := Duration (Election_Duration);
    begin
      accept Start;
      loop
        select
          accept Quit;
          delay Quit_Duration;
        or
          accept Reset_Ticker;
          delay ET;
        or
          delay ET;
          if Role = Follower then
            Current_Term := Current_Term + 1;
            Start_Election;
          end if;
          null;
        end select;
      end loop;
    exception
      when Event : others =>
        GNATCOLL.Traces.Trace (Stream, Event, Msg => Ada.Exceptions.Exception_Information(Event));
    end Election_Ticker;

    task Heartbeat_Ticker is
    -- Heartbeat_Ticker is the task that coordinates the timer
    -- in which the Server should send a hearbeat. Heartbeats
    -- can only be sent by Leaders.
      entry Start;
      entry Quit;
    end Heartbeat_Ticker;

    task body Heartbeat_Ticker is
    begin
      accept Start;
      loop
        select
          accept Quit;
          delay Quit_Duration;
        or
          when Role = Leader =>
          -- delay Reset_Ticker (G, Heartbeat_Duration);
          delay Duration (Heartbeat_Duration);
            for P in Peers_Hash.Iterate (Peers.all) loop
              if Peers_Hash.Key (P) /= Server_Id then
                declare
                  NI : Natural := Next_Index (Peers_Hash.Key (P));
                  Entries : Log_Entry := Get_Entry (Next_Index (Peers_Hash.Key (P)), Log);
                  Response : Append_Entries_Response;
                  Prev_Log_Index : Natural := Get_Prev_Log_Index (NI, Log);
                  Prev_Log_Term : Natural := Get_Prev_Log_Term (NI, Log);
                begin
                  select
                    Peers.all (P).Append_Entries (Current_Term, Server_Id, Prev_Log_Index, Prev_Log_Term, Entries, Commit_Index, Response);
                    -- GNATCOLL.Traces.Trace (Stream, "NI: " & NI'Image);
                    if not Response.Success then
                      Current_Term := Response.Term;
                      Role := Follower;
                    end if;
                  or
                    delay Duration (Heartbeat_Duration);
                    null;
                  end select;
                end;
              end if;
            end loop;
        or
          -- delay Reset_Ticker (G, Heartbeat_Duration);
          delay Duration (Heartbeat_Duration);
          null;
        end select;
      end loop;
    exception
      when Event : others =>
        GNATCOLL.Traces.Trace (Stream, Event, Msg => Ada.Exceptions.Exception_Information(Event));
    end Heartbeat_Ticker;
  begin
    accept Set_Configuration (Cluster : in Peers_Map_Access) do
      for Peer in Peers_Hash.Iterate (Cluster.all) loop
        case Peers_Hash.Key (Peer) = Server_Id is
          when True => null;
          when False => Peers.Insert (Peers_Hash.Key (Peer), Cluster.all (Peer));
        end case;
      end loop;
    end Set_Configuration;
    accept Start;
    Election_Ticker.Start;
    Heartbeat_Ticker.Start;
    loop
      select
        accept Start_Election do
          declare
            Number_Of_Votes : Natural := 1;
          begin
            Role := Candidate;
            GNATCOLL.Traces.Trace (Stream, "Started an election! Term:" & Current_Term'Image);
            for P in Peers_Hash.Iterate (Peers.all) loop
              declare
                Response : Request_Vote_Response := (Term => Current_Term, Vote_Granted => False);
              begin
                select
                    Peers.all (P).Request_Vote (Current_Term, Id, Response);
                or
                  delay Duration (Election_Duration);
                  null;
                end select;
                if Response.Term > Current_Term then
                  Current_Term := Response.Term;
                  Role := Follower;
                  exit;
                end if;
                if Response.Vote_Granted then
                  Number_Of_Votes := Number_Of_Votes + 1;
                end if;
                if Number_Of_Votes > (Natural (Peers_Hash.Length (Peers.all)) / 2) then
                  Role := Leader;
                  Current_Leader := null;
                  Init_Next_Index (Next_Index, Log, Peers);
                  GNATCOLL.Traces.Trace (Stream, "Became the leader!");
                  exit;
                end if;
              end;
            end loop;
            if Role = Candidate then
              Role := Follower;
              GNATCOLL.Traces.Trace (Stream, "Election ended unsuccessfully.");
            end if;
          end;
        end Start_Election;
      or
        accept Append_Entries (Term, Leader_Id, Prev_Log_Index, Prev_Log_Term : in Natural; E : in Log_Entry; Leader_Commit : in Natural; Response : out Append_Entries_Response) do
          case Term >= Current_Term is
            when True =>
              GNATCOLL.Traces.Trace (Stream, "Received a heartbeat from Server" & Leader_Id'Image);
              Current_Term := Term;
              Voted_For := 0;
              Response.Success := True;
              Response.Term := Current_Term;
              Role := Follower;
              Current_Leader := Peers.all (Leader_Id);
              Election_Ticker.Reset_Ticker;
            when False =>
              GNATCOLL.Traces.Trace (Stream, "Rejected a heartbeat from Server" & Leader_Id'Image);
              Response.Success := False;
              Response.Term := Current_Term;
          end case;
        end Append_Entries;
      or
        accept Request_Vote (Term : in Natural; Candidate_Id : in Natural; Response : out Request_Vote_Response) do
          GNATCOLL.Traces.Trace (Stream, "Term:" & Current_Term'Image);
          GNATCOLL.Traces.Trace (Stream, "Candidate ID:" & Candidate_Id'Image & " Term:" & Term'Image);
          case Term > Current_Term and Voted_For = 0 is
            when True =>
              Response.Vote_Granted := True;
              Response.Term := Current_Term;
              Voted_For := Candidate_Id;
              GNATCOLL.Traces.Trace (Stream, "Conceded its vote to Server" & Candidate_Id'Image);
            when False =>
              GNATCOLL.Traces.Trace (Stream, "Denied its vote to Server" & Candidate_Id'Image);
              Response.Vote_Granted := False;
              Response.Term := Current_Term;
          end case;
        end Request_Vote;
      or
        accept Issue_Command do
          case Role = Leader is
            when False =>
              Current_Leader.Issue_Command;
            when True =>
              declare
                Command : Log_Entry := (Term => Current_Term, Is_Empty => False);
              begin
                Log.Append (Command);
                Print_Log (Log, Stream);
              end;
          end case;
        end Issue_Command;
      or
        accept Quit;
        Election_Ticker.Quit;
        Heartbeat_Ticker.Quit;
        delay Quit_Duration;
      end select;
    end loop;
  exception
    when Event : others =>
      GNATCOLL.Traces.Trace (Stream, Event, Msg => Ada.Exceptions.Exception_Information(Event));
  end Server;

  function Id_Hashes (Id : Natural) return Ada.Containers.Hash_Type is
  begin
    return Ada.Strings.Hash (Id'Image);
  end Id_Hashes;

  function Reset_Ticker (G : in out Ada.Numerics.Float_Random.Generator; F : Float) return Duration is
  begin
    return Duration (Ada.Numerics.Float_Random.Random (G) * F + F);
  end Reset_Ticker;

  procedure Init_Next_Index (NI : in out Next_Index_Type.Map; Log : in Log_Vector.Vector; Peers : in Peers_Map_Access) is
    Log_Length : Natural := Natural (Log.Length);
  begin
    for P in Peers_Hash.Iterate (Peers.all) loop
      declare
        Peer_Id : Natural := Peers_Hash.Key (P);
      begin
        case Next_Index_Type.Contains (NI, Peer_Id) is
          when False => NI.Insert (Key => Peer_Id, New_Item => Log_Length);
          when True => NI.Replace (Key => Peer_Id, New_Item => Log_Length);
        end case;
      end;
    end loop;
  end Init_Next_Index;

  function Get_Last_Log_Term (Log : in Log_Vector.Vector) return Natural is
  begin
    case Log.Length is
      when 0 => return 0;
      when others => return Log (Log.Last).Term;
    end case;
  end Get_Last_Log_Term;

  function Get_Prev_Log_Index (NI : in Natural; Log : in Log_Vector.Vector) return Natural is
  begin
    if Natural (Log.Length) = 0 or NI = 0 then
      return 0;
    end if;

    return NI - 1;
  end Get_Prev_Log_Index;

  function Get_Prev_Log_Term (NI : in Natural; Log : in Log_Vector.Vector) return Natural is
  begin
    if Natural (Log.Length) = 0 or NI = 0 then
      return 0;
    end if;

    return Log (NI - 1).Term;
  end Get_Prev_Log_Term;

  function Get_Entry (NI : in Natural; Log : in Log_Vector.Vector) return Log_Entry is
    E : Log_Entry := Log_Entry'(Is_Empty => True, others => <>);
  begin
    if NI < Natural (Log.Length) then
      E := Log (NI);
    end if;
    return E;
  end Get_Entry;

  procedure Print_Log (Log : in Log_Vector.Vector; Stream : in GNATCOLL.Traces.Trace_Handle) is
    use Log_Vector;
  begin
    GNATCOLL.Traces.Trace (Stream, "{");
    for I in Iterate (Log) loop
      declare
        E : Log_Entry := Log (I);
      begin
        if Last (Log) = I then
          GNATCOLL.Traces.Trace (Stream, "  {Term: " & E.Term'Image & "}");
        else
          GNATCOLL.Traces.Trace (Stream, "  {Term: " & E.Term'Image & "},");
        end if;
      end;
    end loop;
    GNATCOLL.Traces.Trace (Stream, "}");
  end Print_Log;
end Raft;
