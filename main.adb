with Ada.Text_IO;
with Ada.Exceptions;
with GNATCOLL.Traces;

with Raft;

procedure Main is
  Cluster : Raft.Peers_Map_Access := new Raft.Peers_Hash.Map;
  S1 : Raft.Server_Access := new Raft.Server (1);
  S2 : Raft.Server_Access := new Raft.Server (2);
  S3 : Raft.Server_Access := new Raft.Server (3);
begin
  GNATCOLL.Traces.Parse_Config_File;

  Cluster.Insert (Key => 1, New_Item => S1);
  Cluster.Insert (Key => 2, New_Item => S2);
  Cluster.Insert (Key => 3, New_Item => S3);

  S1.Set_Configuration (Cluster);
  S2.Set_Configuration (Cluster);
  S3.Set_Configuration (Cluster);

  S1.Start;
  S2.Start;
  S3.Start;

  delay 1.0;
  S1.Issue_Command;
  S1.Issue_Command;
  delay 1.0;
  S1.Quit;
  delay 5.0;
  S1.Issue_Command;
  -- delay 2.0;
  -- S1.Quit;

exception
  when Event : others =>
    Ada.Text_IO.Put_Line (Ada.Exceptions.Exception_Name(Event));
    Ada.Text_IO.Put_Line (Ada.Exceptions.Exception_Message(Event));
    return;
end Main;
