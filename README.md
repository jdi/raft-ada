# Raft implementation in Ada

The objective of this project is to start bootstrapping a Raft implementation using Ada. More specifically, and since I haven't yet decided whether I want to use Ada or not for the full implementation of the algorithm, in this repository I'll be focusing only on the leader election part of Raft.

Together with this repository, there will be other ones where I'll be experimenting with other programming languages until I find the one that better fits what I expect from it [the language] so that I can continue with the full implementation.

## What is here to see?

For running the project just compile the `build.gpr` file using the following `gnat` command on the root folder (please make sure that you have a folder called `obj` on the root directory):

```
$ gprbuild build.gpr
```

After that, you can just run the binary file:

```
$ ./main
```

## How to run the tests

For running the tests you'll need to compile the `tests/harness.gpr` file using the same `gnat` command you used for compiling the project. Please make sure that you have a folder called `obj` inside the `tests` folder. For completeness sake:

```
$ gprbuild tests/harness.gpr
```

After that, you can just run the following binary file:

```
$ ./tests/test_raft
```

In `main.adb` you'll find the main procedure (as the name suggest) while on `server.ads` and `server.adb` you'll encounter the specification and body (respectively) of the server. In those last two files is where the "meat" of the project lives.

## Other implementations

- [Pony](https://gitlab.com/jdi/raft-pony)
- [Go](https://gitlab.com/jdi/raft-go)
