with Server.Test;
with AUnit.Test_Caller;

package body Server_Suite is

   package Caller is new AUnit.Test_Caller (Server.Test.Test);

   function Suite return Access_Test_Suite is
      Result : constant Access_Test_Suite := new Test_Suite;
   begin
      Result.Add_Test
        (Caller.Create ("Test addition", Server.Test.Test_Addition'Access));
      Result.Add_Test
        (Caller.Create ("Test subtraction", Server.Test.Test_Subtraction'Access));
      return Result;
   end Suite;

end Server_Suite;
