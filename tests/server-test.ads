with AUnit;
with AUnit.Test_Fixtures;

package Server.Test is

   type Test is new AUnit.Test_Fixtures.Test_Fixture with record
      I1 : Integer;
      I2 : Integer;
   end record;

   procedure Set_Up (T : in out Test);

   procedure Test_Addition (T : in out Test);
   procedure Test_Subtraction (T : in out Test);

end Server.Test;
