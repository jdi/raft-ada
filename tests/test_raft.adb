with AUnit.Reporter.Text;
with AUnit.Run;
with Server_Suite; use Server_Suite;

procedure Test_Raft is
   procedure Runner is new AUnit.Run.Test_Runner (Suite);
   Reporter : AUnit.Reporter.Text.Text_Reporter;
begin
   Runner (Reporter);
end Test_Raft;
